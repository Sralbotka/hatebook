import Typography from "@mui/material/Typography";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import Box from "@mui/system/Box";
import {useEffect, useState} from "react";
import {FriendRequest} from "../serverCalls/ServerUtils";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import {chat, ChatMessage, registerMessageListener} from "../serverCalls/STOMP";

export interface ChatProps{
    friends:FriendRequest[]
    from:string
}

export let chatMap:Record<string, string[]> = {};

export function fillChatMessages(messages:ChatMessage[], email:string) {
    chatMap = {};
    messages.forEach(value => {
        if(value.from !== email) {
            if(!chatMap[value.from]) chatMap[value.from] = [];
            chatMap[value.from].push(value.message);
        } else {
            if(!chatMap[value.to]) chatMap[value.to] = [];
            chatMap[value.to].push(value.message);
        }
    });
}

export function Chat(props:ChatProps){
    const[messages, setMessage] = useState<string[]>([]);
    const[to, setTo] = useState("");

    function incomingMessage(message:ChatMessage){
        if (!chatMap[message.from]) chatMap[message.from] = [];
        chatMap[message.from].push(message.message);
        console.log("---RECEIVED---" + message.from);
        console.log("TO:" + to);
        console.log("UPDATING");
        setMessage([...chatMap[message.from].concat(chatMap[props.from])]);
    }

    useEffect(() => {
        registerMessageListener(incomingMessage);
    }, []);

    function handleSubmit(event: React.FormEvent<HTMLFormElement>){
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        let post = {
            message: data.get("content")+"",
            seconds:Date.now()/1000,
            to:to
        }
        if(!chatMap[to]) chatMap[to] = [];
        chatMap[to].push(post.message);
        setMessage([...chatMap[to]]);
        chat({from:props.from, message: post.message, to:to, timestamp: post.seconds})
        console.log(chatMap);
    }

    function changeChat(email:string){
        if(!chatMap[email]) chatMap[email]=[];
        setMessage(chatMap[email]);
        setTo(email);
    }

    function onChangeChat(email:string){
        return () => changeChat(email);
    }

    return(
        <Box sx={{
            display:'flex',
            flexDirection: 'row',
        }}>
            <List>
                {props.friends.map((friend) => <ListItem key={friend.minutes+"-item"}><ListItemButton onClick={onChangeChat(friend.friend.email)} key={friend.minutes+"-button"}><ListItemText primary={friend.friend.name} key={friend.minutes+"-text"}></ListItemText></ListItemButton></ListItem>)}
            </List>
            <Box sx={{
                display:'flex',
                flexDirection:'column'
            }}>
                {messages.map((message, index) => <Typography key={`message-${index}`}>{message}</Typography>)}
                <Box component="form" onSubmit={handleSubmit} sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}>
                    <TextField
                        id="conent"
                        label="Hate 1:1"
                        placeholder="Hate 1:1"
                        name="content"
                        multiline
                        fullWidth={true}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{ mt: 3, mb: 2 }}
                    >    Send your hate
                    </Button>
                </Box>
            </Box>
        </Box>
    )


}