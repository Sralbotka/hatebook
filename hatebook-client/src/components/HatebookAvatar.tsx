import {red} from "@mui/material/colors";
import {nameForAvatar} from "../utilities/UserUtils";
import Avatar from '@mui/material/Avatar';

export interface AvatarProps {
  who: string
}

export function HatebookAvatar(props: AvatarProps) {
  return (
    <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
      {nameForAvatar(props.who)}
    </Avatar>);
}