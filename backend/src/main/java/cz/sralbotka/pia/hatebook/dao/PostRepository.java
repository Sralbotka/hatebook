package cz.sralbotka.pia.hatebook.dao;

import cz.sralbotka.pia.hatebook.entities.RegularPost;
import cz.sralbotka.pia.hatebook.entities.User;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PostRepository extends PagingAndSortingRepository<RegularPost, Long> {
    List<RegularPost> findByAuthor(User author);
}
