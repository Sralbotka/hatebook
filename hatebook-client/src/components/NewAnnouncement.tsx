import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import TextField from "@mui/material/TextField";
import React from "react";
import {PostType} from "./Feed";

export interface NewPostProps {
    onClose: () => void,
    addPost: (post: PostType) => void,
    open: boolean
}
export function NewAnnouncement(props: NewPostProps) {

    function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        let post = {
            author: "Current User",
            title: data.get("title")+"",
            message: data.get("content")+"",
            seconds:Date.now()/1000
        }
        props.addPost(post);
        props.onClose();
    }


    return (<Dialog onClose={props.onClose} open={props.open} fullWidth={true}
                    maxWidth="lg" >
        <DialogTitle>Add a announcement</DialogTitle>
        <Box component="form" onSubmit={handleSubmit} sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
        }}>
            <TextField
                id="title"
                label="Enter title here"
                name="title"
                placeholder="Enter title here"
                fullWidth={true}
            />
            <TextField
                id="conent"
                label="Enter content here"
                placeholder="Enter content here"
                name="content"
                multiline
                fullWidth={true}
            />
            <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
            >    Announce hatred
            </Button>
        </Box>
    </Dialog>);
}