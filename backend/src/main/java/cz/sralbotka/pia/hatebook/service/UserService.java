package cz.sralbotka.pia.hatebook.service;

import cz.sralbotka.pia.hatebook.beans.TokenBean;
import cz.sralbotka.pia.hatebook.dao.UserRepository;
import cz.sralbotka.pia.hatebook.entities.Permission;
import cz.sralbotka.pia.hatebook.entities.Role;
import cz.sralbotka.pia.hatebook.entities.User;
import cz.sralbotka.pia.hatebook.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Service
public class UserService implements IUserService {


    private final UserRepository userDAO;
    private final BCryptPasswordEncoder encoder;
    private final IFulltextSearchService fulltextService;
    @Autowired
    private Environment env;

    public final static String NICE_TRY = "Nice try, try again...";

    private void notImplemeted() {
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "NOT IMPLEMENTED YET");
    }

    public User findUserById(String email) {
        return userDAO.findById(email).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can't find user by email"));
    }

    @Override
    public User getUserFromToken() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            return userDAO.findById(String.valueOf(authentication.getPrincipal())).orElseThrow(() -> new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Not authenticated"));
        }
        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Not authenticated");
    }

    @Override
    public boolean isAdminGranted() {
        for (User user : userDAO.findAll()) {
            if (user.containsAdmin()) {
                return true;
            }
        }
        return false;
    }

    public UserService(UserRepository userDAO, BCryptPasswordEncoder encoder, IFulltextSearchService fulltextService) {
        this.userDAO = userDAO;
        this.encoder = encoder;
        this.fulltextService = fulltextService;
    }


    @Override
    public void addUser(User user) {
        if (userDAO.findById(user.getEmail()).isEmpty()) {
            user.setPassword(encoder.encode(user.getPassword()));
            user.setPermission(isAdminGranted() ? Permission.USER : Permission.ADMIN);
            userDAO.save(user);
        } else throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Sorry dude email taken already");
    }

    @Override
    public List<User> searchUsers(String nameSearch) {
        User fromToken = getUserFromToken();
        return fulltextService.fulltextFindUsers(nameSearch)
                .filter((user) -> !fromToken.getPeopleThatBannedMe().contains(user))
                .collect(Collectors.toList());

    }

    @Override
    public boolean banUser(String email) {
        User currentUser = getUserFromToken();
        User toBan = findUserById(email);
        if(toBan != null) {
            currentUser.addPeopleIBanned(toBan);
            userDAO.saveAll(List.of(currentUser, toBan));
            return true;
        } else {
            return false;
        }
    }

    private Supplier<ResponseStatusException> status(HttpStatus status, String exceptionText) {
        return () -> new ResponseStatusException(status, exceptionText);
    }

    @Override
    public boolean unbanUser(String email) {
        User currentUser = getUserFromToken();
        User toBan = findUserById(email);
        currentUser.removePeopleIBanned(toBan);
        userDAO.saveAll(List.of(currentUser, toBan));
        return true;
    }

    @Override
    @Transactional
    public void timestampUser(User user) {
        user.setLastSeen(System.currentTimeMillis() / 1000);
        userDAO.save(user);
    }

    @Override
    public void promoteUser(String email) {
        User user = findUserById(email);
        if (user.getPermission() == Permission.USER) {
            user.setPermission(Permission.ADMIN);
            userDAO.save(user);
            return;
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, NICE_TRY);
    }

    //TODO set them together
    @Override
    public void demoteUser(String email) {
        User user = findUserById(email);
        if (user.getPermission() == Permission.ADMIN) {
            user.setPermission(Permission.USER);
            userDAO.save(user);
            return;
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, NICE_TRY);
    }

    @Override
    public TokenBean authUser(String email, String password, Permission permission) {
        User user = findUserById(email);
        if (encoder.matches(password, user.getPassword())) {
            timestampUser(user);
            return new TokenBean(SecurityUtils.createJWTToken(email), user.getName(), user.getEmail(), user.getPermission());
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, NICE_TRY);
    }

    @Override
    public boolean isEmailTaken(String email) {
        return userDAO.findById(email).isPresent();
    }

    @PostConstruct
    private void createAdminIfNecessary() {
        if (!isAdminGranted()) {
            Iterator<User> users = userDAO.findAll().iterator();
            if (users.hasNext()) {
                User u = users.next();
                u.setPermission(Permission.ADMIN);
                userDAO.save(u);
            } else {
                String default_name = env.getProperty("user.default.name");
                String default_email = env.getProperty("user.default.email");
                String plaintext_password_idk_why_we_should_use_it = env.getProperty("user.default.passwd");
                User defaultUser = new User(default_email, default_name, plaintext_password_idk_why_we_should_use_it, generateDefaultRoles(), Long.parseLong("0"));
                defaultUser.setPermission(Permission.ADMIN);
                Role admin = new Role(Permission.ADMIN.name());
                defaultUser.getRole().add(admin);
                userDAO.save(defaultUser);
            }
        }
    }

    private Set<Role> generateDefaultRoles() {
        Set<Role> roles = new HashSet<>();
        roles.add(new Role(Permission.USER.name()));
        return roles;
    }

    @org.springframework.transaction.annotation.Transactional
    public void removeTimestamp(User user) {
        user.setLastSeen(0);
        userDAO.save(user);
    }
}



