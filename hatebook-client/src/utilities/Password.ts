function scorePassword(pass:string) {
    let score = 0;
    if (!pass)
        return score;

    // award every unique letter until 5 repetitions
    let letters:any = {};
    for (var i=0; i<pass.length; i++) {
        letters[pass[i]] = (letters[pass[i]] || 0) + 1;
        score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up
    let variations:any = {
        digits: /\d/.test(pass),
        lower: /[a-z]/.test(pass),
        upper: /[A-Z]/.test(pass),
        nonWords: /\W/.test(pass),
    }

    let variationCount = 0;
    for (let check in variations) {
        variationCount += (variations[check] === true) ? 1 : 0;
    }
    score += (variationCount - 1) * 10;

    return score;
}

export interface EntropyType{
    score: number,
    translation : string
}

export function checkEntropy(password:string):EntropyType{
    let score = scorePassword(password);
    let translation = "shit";
    
    if (score > 80)
        translation = "strong";
    else if (score > 60)
        translation =  "good";
    else if (score >= 30)
        translation =  "weak";

    return {score : score, translation: translation}
}