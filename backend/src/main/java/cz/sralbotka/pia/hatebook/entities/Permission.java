package cz.sralbotka.pia.hatebook.entities;

public enum Permission {
    ADMIN, USER
}
