package cz.sralbotka.pia.hatebook.chat;

import cz.sralbotka.pia.hatebook.service.MessagingService;
import cz.sralbotka.pia.hatebook.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;
import org.springframework.web.socket.messaging.SessionUnsubscribeEvent;

public class ChatListener {
    private final Logger logger = LoggerFactory.getLogger(ChatListener.class);

    @Autowired
    private UserService userService;

    @Autowired
    private MessagingService service;

    private SimpMessagingTemplate messagingTemplate;

    ChatListener( SimpMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @EventListener
    public void handleSessionSubscribeEvent(SessionSubscribeEvent event) {
        SimpMessageHeaderAccessor headers = SimpMessageHeaderAccessor.wrap(event.getMessage());
        if(headers.getDestination().startsWith("/topic/chat")) {
            String email = headers.getDestination().substring(headers.getDestination().lastIndexOf("/") + 1, headers.getDestination().length());
            service.addUser(userService.findUserById(email), headers);
        }
        logger.trace(event.toString());
    }

    @EventListener
    public void handleSessionUnsubscribeEvent(SessionUnsubscribeEvent event) {
        logger.trace(event.toString());
    }

    @EventListener
    public void handleSessionDisconnect(SessionDisconnectEvent event) {
        service.disconectUser(event.getSessionId());
    }
}
