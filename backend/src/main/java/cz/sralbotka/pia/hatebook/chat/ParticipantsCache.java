package cz.sralbotka.pia.hatebook.chat;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class ParticipantsCache {
    Map<String, LoginEvent> activeUsers = new ConcurrentHashMap<>();

    public void add(String sessionId, LoginEvent event) {
        activeUsers.put(sessionId, event);
    }

    public LoginEvent getParticipant(String sessionId) {
        return activeUsers.get(sessionId);
    }

    public void removeParticipant(String sessionId) {
        activeUsers.remove(sessionId);
    }

    public Map<String, LoginEvent> getActiveSessions() {
        return activeUsers;
    }

    public void setActiveSessions(Map<String, LoginEvent> activeSessions) {
        this.activeUsers = activeSessions;
    }
}
