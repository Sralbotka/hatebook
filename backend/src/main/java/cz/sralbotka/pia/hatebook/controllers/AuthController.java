package cz.sralbotka.pia.hatebook.controllers;

import cz.sralbotka.pia.hatebook.beans.TokenBean;
import cz.sralbotka.pia.hatebook.entities.User;
import cz.sralbotka.pia.hatebook.security.SecurityUtils;
import cz.sralbotka.pia.hatebook.service.IUserService;
import cz.sralbotka.pia.hatebook.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

/**
 * A controller for authentication -> can register user, login user, and reset password (COMING SOON)
 */

@RestController
public class AuthController extends AbstractUserController {


    /**
     * Constructor that is autowired by IOC
     *
     * @param userService - service that takes care of all related to users
     *
     */
    public AuthController(IUserService userService) {
        super(userService);
    }

    /**
     * Registers user on a route /register
     *
     * @param user - parsed from json - requires email that is not in the DB and password that exists (client will do the entropy check though)
     *             meaning if you are hacker and want to have a shitty password - please go ahead, it's your data in the end... Sorry...
     * @return - 200 when user was created, 400 when not
     */
    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ResponseEntity<String> register(@RequestBody User user) {
        if (hasEmailNameAndPassword(user)) {
            userService.addUser(user);
            return ResponseEntity.status(HttpStatus.OK).body("Your hate will grow stronger " + user.getName());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Woooha your hatred is not enough!");

    }

    /**
     * Logins a user to the system and gets a token that is valid for a time specified in {@link SecurityUtils#EXPIRATION_TIME}
     *
     * @param user - user to login (bean must match JSON provided in the body - email and password are mandatory rest can be whatever)
     * @return - 200 and token if everything works nicely - email and password matches, 400 and sassy message when not
     */
    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public ResponseEntity<TokenBean> login(@RequestBody User user) {
        if(hasValidEmail(user) && user.getPassword()!= null){
            return ResponseEntity.status(200).body(userService.authUser(user.getEmail(), user.getPassword(), user.getPermission()));
        }
        else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, UserService.NICE_TRY);
        }
    }

    @RequestMapping(path="/emailTaken", method = RequestMethod.GET)
    public ResponseEntity<Boolean> checkEmail(@RequestParam("email") String email){
        return ResponseEntity.status(HttpStatus.OK).body(userService.isEmailTaken(email));
    }

}
