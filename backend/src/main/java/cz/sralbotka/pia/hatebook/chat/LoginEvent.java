package cz.sralbotka.pia.hatebook.chat;

import cz.sralbotka.pia.hatebook.entities.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class LoginEvent {
    private User user;
    private long timestamp;
}
