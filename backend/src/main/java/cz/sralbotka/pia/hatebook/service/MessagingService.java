package cz.sralbotka.pia.hatebook.service;

import cz.sralbotka.pia.hatebook.beans.MessageBean;
import cz.sralbotka.pia.hatebook.chat.LoginEvent;
import cz.sralbotka.pia.hatebook.chat.ParticipantsCache;
import cz.sralbotka.pia.hatebook.dao.MessageRepository;
import cz.sralbotka.pia.hatebook.entities.Message;
import cz.sralbotka.pia.hatebook.entities.User;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class MessagingService implements IMessagingService{
    UserService userService;
    FriendshipService friendshipService;
    private MessageRepository repo;
    private SimpMessagingTemplate msgTemplate;

    @Autowired
    private ParticipantsCache cache;

    private final Logger logger = LoggerFactory.getLogger(MessagingService.class);

    public void addMessage(String from, String to, String message) {
        repo.save(new Message(userService.findUserById(from), userService.findUserById(to), System.currentTimeMillis() / 1000, message));
    }

    public List<MessageBean> getChatHistory(String email) {
        User currentUser = userService.findUserById(email);
        if(currentUser!=null) {
            List<Message> messages = new ArrayList<>();
            List<Message> finalMessages;
            friendshipService.getFriends().forEach(user -> {
                messages.addAll(repo.findTop10ByToEmailAndFromEmailOrderBySecondsDesc(email, user.getEmail())); messages.addAll(repo.findTop10ByFromEmailAndToEmailOrderBySecondsDesc(email, user.getEmail()));}
            );
            messages.sort(Comparator.comparingLong(Message::getSeconds).reversed());
            finalMessages = messages.subList(0, Math.min(messages.size(), 10));
            finalMessages.sort(Comparator.comparingLong(Message::getSeconds));
            return convertToMessageBeans(finalMessages);
        }
        return Collections.emptyList();
    }

    private List<MessageBean> convertToMessageBeans(List<Message> messages) {
        return messages.stream().map(getBeanFromMessage()).collect(Collectors.toList());
    }


    private Function<Message, MessageBean> getBeanFromMessage() {
        return (message) -> new MessageBean(message.getFrom().getEmail(), message.getTo().getEmail(), message.getSeconds(), message.getMessage());
    }

    public void notifyFriends() {
        User u = userService.getUserFromToken();
        userService.timestampUser(u);
        friendshipService.getFriends().forEach(
                user -> msgTemplate.convertAndSend("/topic/chat/online/"+ user.getEmail(), true)
        );
    }

    public void notifyFriends(User u) {
        friendshipService.getFriends(u).forEach(
            user -> msgTemplate.convertAndSend("/topic/chat/online/"+ user.getEmail(), true)
        );
    }

    public void addUser(User u, SimpMessageHeaderAccessor headers) {
        cache.add(headers.getSessionId(), new LoginEvent(u, System.currentTimeMillis() / 1000));
    }

    public void disconectUser(String sessionId) {
        LoginEvent e = cache.getParticipant(sessionId);
        if(e != null) {
            userService.removeTimestamp(e.getUser());
            cache.removeParticipant(sessionId);
            notifyFriends(e.getUser());
        }
        logger.trace(sessionId);
    }
}
