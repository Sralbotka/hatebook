package cz.sralbotka.pia.hatebook.dao;

import cz.sralbotka.pia.hatebook.entities.Friendship;
import cz.sralbotka.pia.hatebook.entities.FriendshipKey;
import cz.sralbotka.pia.hatebook.entities.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FriendshipRepository extends CrudRepository<Friendship, FriendshipKey> {
    List<Friendship> findByRequester(User requester);
    List<Friendship> findByConfirmer(User confirmer);

    Friendship findByConfirmerAndRequesterAndConfirmed(User requester, User confirmer, boolean confirmed);
}
