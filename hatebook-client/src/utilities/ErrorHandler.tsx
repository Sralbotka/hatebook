import Alert from "@mui/material/Alert";
import {displaySnack} from "../App";

export function displayError(error:string){    
    displaySnack(<Alert severity="error">{error}</Alert>);
}

export function displaySuccess(message:string){
    displaySnack(<Alert severity="success">{message}</Alert>);
    
}