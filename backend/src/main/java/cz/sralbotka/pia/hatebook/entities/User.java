package cz.sralbotka.pia.hatebook.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Indexed
@Table(name = "haters")
public class User {

    protected User() {
        this.lastSeen = (long) 0;
    }

    public User(String email, String name, String password, Set<Role> roles, Long lastSeen) {
        this.email = email;
        this.name = name;
        this.roles = roles;
        this.password = password;
        if (this.roles.isEmpty()) {
            this.roles.add(new Role(Permission.USER.name()));
        }
        this.lastSeen = lastSeen;
    }

    @Id
    private String email;
    @Field
    private String name;
    private String password;
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "haters_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    Set<Role> roles;

    @Transient
    private Permission permission = Permission.USER;

    private Long lastSeen;


    @ManyToMany()
    @JoinTable(name = "haters_bans",
            joinColumns = @JoinColumn(name = "userId"),
            inverseJoinColumns = @JoinColumn(name = "banId")
    )
    private Set<User> blockedByMe = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "haters_bans",
            joinColumns = @JoinColumn(name = "banId"),
            inverseJoinColumns = @JoinColumn(name = "userId")
    )
    @JsonIgnore
    private Set<User> peopleThatBannedMe = new HashSet<>();

    @ManyToMany()
    private Set<User> friends = new HashSet<>();

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRole() {
        return roles;
    }

    public Permission getPermission() {
        if (this.containsAdmin()) {
            return Permission.ADMIN;
        } else {
            return Permission.USER;
        }
    }

    public void setPermission(Permission permission) {
        if (this.roles == null) {
            this.roles = new HashSet<>();
        }
        this.roles.add(new Role(permission.name()));
        if(!this.containsAdmin() && permission == Permission.ADMIN) {
            this.permission = Permission.ADMIN;
        } else {
            this.permission = Permission.USER;
        }
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public long getLastSeen() {
        return lastSeen;
    }

    public boolean containsAdmin() {
        if (this.roles == null) {
            this.roles = new HashSet<>();
        }
        return roles.contains(new Role(Permission.ADMIN.name()));
    }

    public void setLastSeen(long lastOnline) {
        this.lastSeen = lastOnline;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (getEmail() != null ? !getEmail().equals(user.getEmail()) : user.getEmail() != null) return false;
        if (getName() != null ? !getName().equals(user.getName()) : user.getName() != null) return false;
        return getPassword() != null ? getPassword().equals(user.getPassword()) : user.getPassword() == null;
    }

    @Override
    public int hashCode() {
        return getEmail() != null ? getEmail().hashCode() : 0;
    }

    public Set<User> getPeopleThatBannedMe() {
        return peopleThatBannedMe;
    }

    public Set<User> getBlockedByMe() {
        return blockedByMe;
    }

    public void setBlockedByMe(Set<User> peopleIBanned) {
        this.blockedByMe = peopleIBanned;
    }

    public void setPeopleThatBannedMe(Set<User> peopleThatBannedMe) {
        this.peopleThatBannedMe = peopleThatBannedMe;
    }

    public void addPeopleIBanned(User u) {
        blockedByMe.add(u);
    }

    public void removePeopleIBanned(User u) {
        blockedByMe.remove(u);
    }
}
