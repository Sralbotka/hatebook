package cz.sralbotka.pia.hatebook.dao;

import cz.sralbotka.pia.hatebook.entities.AnnouncementPost;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AnnouncementRepository extends PagingAndSortingRepository<AnnouncementPost, Long> {
    List<AnnouncementPost> findAll();
}
