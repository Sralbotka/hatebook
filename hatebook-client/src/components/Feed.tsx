import Avatar from "@mui/material/Avatar";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";

import React, {useEffect, useState} from "react";
import {red} from '@mui/material/colors';
import {nameForAvatar} from "../utilities/UserUtils";
import {Post} from "./Post";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import {NewPost, NewPostProps} from "./NewPost";
import {addAnnouncement, addPost} from "../serverCalls/Auth";
import {AxiosResponse} from "axios";
import {reRoute} from "./Navigator";
import {Permission, UserInfo} from "../serverCalls/ServerUtils";
import Typography from "@mui/material/Typography";
import {registerPostRefreshListener} from "../serverCalls/STOMP";
import {NewAnnouncement} from "./NewAnnouncement";
import Grid from "@mui/material/Grid";
import {PopupChat} from "./PopupChat";

export interface PostType {
    author: string,
    title: string,
    message: string,
    seconds: number
}

export interface FeedProps {
    fetchPosts: (email: string) => Promise<AxiosResponse<PostType[]>>
    email: string,
    auth: UserInfo

}

export function Feed(props: FeedProps) {
    if (props.auth.email.length === 0) reRoute("/");
    const [feeds, setFeeds] = useState<PostType[]>([]);
    const [newPostOpened, setNewPostOpened] = useState(false);
    const [newAnnouncementOpened, setAnnouncementOpened] = useState(false);

    let showGreeting = props.email.length === 0;
    let showAddPost = props.email === '' && showGreeting;
    let showAddAnnouncement = props.email === '' && props.auth.permission === Permission.ADMIN && showGreeting;

    function onClose() {
        setNewPostOpened(false);
        setAnnouncementOpened(false);
    }

    function newPost(): void {
        setNewPostOpened(true);
    }

    function newAnnouncement(): void {
        setAnnouncementOpened(true);
    }

    function doFetch() {
        props.fetchPosts(props.email).then(response => setFeeds(response.data)).catch(e => console.log(e));
    }

    useEffect(() => {
        doFetch();
    }, [props.email])

    useEffect(() => {
        registerPostRefreshListener(() => {
            doFetch();
        });
    }, [])

    function addPostWithRefresh(post: PostType) {
        addPost(post).then(() => doFetch());
    }

    function addAnnouncementWithRefresh(post: PostType) {
        addAnnouncement(post).then(() => doFetch());
    }

    const newPostProps: NewPostProps = {
        onClose: onClose,
        addPost: addPostWithRefresh,
        open: newPostOpened
    }

    const newAnnouncementProps: NewPostProps = {
        onClose: onClose,
        addPost: addAnnouncementWithRefresh,
        open: newAnnouncementOpened
    }

    return (
        <Box sx={{
            display: 'flex',
            flexDirection: 'row',
        }}>
            {/*Those are friends on left*/}
            <PopupChat friends={props.auth.friends} from={props.auth.email}/>
            <Box
                sx={{
                    marginTop: 8,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <NewPost {...newPostProps} />
                <NewAnnouncement {...newAnnouncementProps} />
                <Grid container spacing={12}>
                    {showGreeting && <Grid item md={12}><Card sx={{maxWidth: 600}} key="greeting">
                        <CardContent>
                            <Typography>
                                {`Hello ${props.auth.name} your hatred can be shared by button bellow!`}
                            </Typography>
                        </CardContent>
                    </Card></Grid>}
                    {showAddPost && <Grid item md={6}><Card sx={{maxWidth: 300}} key="newpost">
                        <CardContent>
                            <Box sx={{
                                display: 'flex',
                                flexDirection: 'row',
                                float: 'left'
                            }}>
                                <Avatar sx={{bgcolor: red[500]}} aria-label="Me">
                                    {nameForAvatar(props.auth.name)}
                                </Avatar>
                                <Button onClick={newPost} fullWidth={true}>Add post</Button>
                            </Box>

                        </CardContent>
                    </Card></Grid>
                    }
                    {showAddAnnouncement && <Grid item md={6}><Card sx={{maxWidth: 300}} key="newannoucement">
                        <CardContent>
                            <Box sx={{
                                display: 'flex',
                                flexDirection: 'row',
                                float: 'right'
                            }}>
                                <Avatar sx={{bgcolor: red[500]}} aria-label="Me">
                                    {nameForAvatar(props.auth.name)}
                                </Avatar>
                                <Button onClick={newAnnouncement} fullWidth={true}>Add announcement</Button>
                            </Box>

                        </CardContent>
                    </Card></Grid>
                    }
                </Grid>

                {feeds.map((feed, index) => <Post {...feed} key={feed.seconds + feed.author}/>)}
            </Box>
        </Box>
    );
}