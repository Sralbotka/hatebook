package cz.sralbotka.pia.hatebook.dao;

import cz.sralbotka.pia.hatebook.entities.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Interface that is auto implemented by boot - HOW COOL IS THAT!
 */
public interface UserRepository extends CrudRepository<User, String> {
    /**
     * Finds users by their name
     *
     * @param name - name that must be indexed to have this working faster
     * @return list of users having that kind of a name
     */
    List<User> findByName(String name);
}
