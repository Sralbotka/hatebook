package cz.sralbotka.pia.hatebook.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
@Embeddable
public class FriendshipKey implements Serializable {
    @Column(name = "request_id")
    private String requester;
    @Column(name = "confirm_id")
    private String confirmer;

    public FriendshipKey(String requester, String confirmer) {
        this.requester = requester;
        this.confirmer = confirmer;
    }

    public FriendshipKey() {

    }
}


