package cz.sralbotka.pia.hatebook.entities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Friendship {

    @EmbeddedId
    private FriendshipKey key;
    private long minutes;
    private boolean confirmed;
    @ManyToOne
    @JoinColumn(name="request_id", insertable = false, updatable = false)
    private User requester;
    @ManyToOne
    @JoinColumn(name="confirm_id", insertable = false, updatable = false)
    private User confirmer;

    public Friendship(FriendshipKey key) {
        this.key = key;
        this.minutes = System.currentTimeMillis() / 1000 / 60;
        this.confirmed = false;
    }

    public User getRequester() {
        return requester;
    }

    public User getConfirmer() {
        return confirmer;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public Friendship confirm(){
        this.confirmed = true;
        return this;
    }

    protected Friendship() {

    }
}
