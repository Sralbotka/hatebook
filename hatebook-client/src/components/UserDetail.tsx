import Button from "@mui/material/Button";
import {useParams} from "react-router-dom";
import {getPostsFor} from "../serverCalls/Auth";
import {
    addFriend,
    banUser,
    cancelFriendshipRequest,
    confirmFriend,
    removeFriend,
    unbanUser
} from "../serverCalls/Friendship";
import {FriendRequest, User} from "../serverCalls/ServerUtils";
import {Feed, FeedProps} from "./Feed";

interface ActionButtonProps {
    handler: () => void,
    label: string,
    minutes?: number,
    user?: User

}

export function UserDetail(props: FeedProps) {
    let {id} = useParams<"id">();
    let email: string = !id ? "" : id;

    let actionProps = getActionProps();


    return (
        <div>
            {actionProps !== undefined && actionProps.map(value => {return <Button fullWidth onClick={value.handler}>{value.label}</Button>})}
            <Feed fetchPosts={getPostsFor} email={email} auth={props.auth}/>
        </div>
    )

    function findByEmail(friendship: FriendRequest): boolean {
        return friendship.friend.email === email
    }

    function findUserByEmail(user: User): boolean {
        return user.email === email;
    }


    function onRemoveFriend() {
        removeFriend(email);
    }

    function cancelFriendshipRequestHandler() {
        cancelFriendshipRequest(email);
    }

    function confirmFriendship() {
        confirmFriend(email);
    }

    function addAsFriend() {
        addFriend(email);
    }

    function banHim() {
        banUser(email)
    }

    function unbanHim() {
        unbanUser(email);
    }

    function getActionProps(): ActionButtonProps[] | undefined {
        let actionProps : ActionButtonProps[] = [];
        let info = props.auth;
        console.log(info);
        if (email === info.email) {
            return undefined;
        }
        let friend = info.friends.find(findByEmail);
        if (!!friend) {
            actionProps.push({handler: onRemoveFriend, label: "Remove Friend", minutes: friend.minutes, user: friend.friend})
            return actionProps;
        }
        let banned = info.blockedByMe.find(findUserByEmail);
        if (banned) {
            actionProps.push({handler:unbanHim, label: "Unblock user"})
            return actionProps;
        } else {
            actionProps.push({handler: banHim, label: "Block this user"});
        }
        friend = info.iRequestedTheirConfirmation.find(findByEmail);
        if (!!friend) {
            actionProps.push({
                handler: cancelFriendshipRequestHandler,
                label: "Cancel friendship request",
                minutes: friend.minutes,
                user: friend.friend
            });
            return actionProps;
        }
        friend = info.requestMyConfirmation.find(findByEmail);
        if (!!friend) {
            actionProps.push({
                handler: confirmFriendship,
                label: "Confirm Friendship Request",
                minutes: friend.minutes,
                user: friend.friend
            });
            return actionProps;
        } else {
            actionProps.push({handler: addAsFriend, label: "Add as friend"});
            return actionProps;
        }
    }
}