package cz.sralbotka.pia.hatebook.controllers;


import cz.sralbotka.pia.hatebook.entities.User;
import cz.sralbotka.pia.hatebook.service.IUserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

/**
 * A service implementation for all users
 */
@RestController
@RequestMapping("/ban")
public class BanController extends AbstractUserController{

    public static final String WRONG_EMAIL = "Sorry that is not an email in the body! Expect to obtain {email:a@b.c";

    public BanController(IUserService userService) {
        super(userService);
    }

    /**
     * Returns set of banned people
     * @return Status ok when everything is ok with set of blocked people
     */
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<Set<User>> getBannedPeople() {
        return ResponseEntity.status(HttpStatus.OK).body(userService.getUserFromToken().getBlockedByMe());
    }

    /**
     * bans user (sets user as banned)
     * @param email email of the user to be banned
     * @return 200 if OK | 400 if NOK
     */
    @RequestMapping(value = "/{email}", method = RequestMethod.POST)
    public ResponseEntity<String> ban(@PathVariable String email) {
        if(userService.banUser(email)) {
            return ResponseEntity.status(200).body("Banning success!");
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(WRONG_EMAIL);
        }
    }

    /**
     * unbans user (removes user from ban list)
     * @param email email of the banned user
     * @return 200 if OK | 400 if NOK
     */
    @RequestMapping(value = "/{email}", method = RequestMethod.DELETE)
    public ResponseEntity<String> unban(@PathVariable String email) {
        if(userService.unbanUser(email)) {
            return ResponseEntity.status(200).body("User successfully unblocked");
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(WRONG_EMAIL);
        }
    }
}
