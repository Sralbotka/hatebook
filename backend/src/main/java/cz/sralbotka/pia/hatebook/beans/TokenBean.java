package cz.sralbotka.pia.hatebook.beans;

import cz.sralbotka.pia.hatebook.entities.Permission;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TokenBean {
    private String token;
    private String name;
    private String email;
    private Permission permission;
}
