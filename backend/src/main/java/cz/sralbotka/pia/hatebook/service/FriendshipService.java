package cz.sralbotka.pia.hatebook.service;

import cz.sralbotka.pia.hatebook.dao.FriendshipRepository;
import cz.sralbotka.pia.hatebook.entities.Friendship;
import cz.sralbotka.pia.hatebook.entities.FriendshipKey;
import cz.sralbotka.pia.hatebook.entities.User;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AllArgsConstructor
@Service
public class FriendshipService {
    private FriendshipRepository repo;
    private UserService userService;
    private SimpMessagingTemplate msgTemplate;


    public List<Friendship> getFriendships(){
        User current = userService.getUserFromToken();
        userService.timestampUser(current);
        return Stream.concat(repo.findByConfirmer(current).stream(), repo.findByRequester(current).stream()).collect(Collectors.toList());
    }

    private void pushNotification(List<String> destinations){
        for (String destination : destinations) {
            msgTemplate.convertAndSend("/topic/friendship/"+destination, "UPDATE");
        }
    }

    private void pushNotification(Friendship friendship){
        pushNotification(List.of(friendship.getConfirmer().getEmail(), friendship.getRequester().getEmail()));
    }

    public void makeFriendShip(String toBefriend) {
        User requester = userService.getUserFromToken();

        if (requester.getEmail().equals(toBefriend)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can't befriend yourself...");
        }
        User confirmer = userService.findUserById(toBefriend);

        FriendshipKey correctKey = new FriendshipKey(requester.getEmail(), confirmer.getEmail());

        if (repo.existsById(correctKey) || repo.existsById(new FriendshipKey(confirmer.getEmail(), requester.getEmail()))) {

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Friendship already exists!");
        }
        repo.save(new Friendship(correctKey));
        pushNotification(List.of(requester.getEmail(), confirmer.getEmail()));
    }

    public List<User> getFriends() {
        User requester = userService.getUserFromToken();
        List<User> friends = repo.findByRequester(requester).stream().filter(friendship -> friendship.isConfirmed()).map(friendship -> friendship.getConfirmer()).collect(Collectors.toList());
        friends.addAll(repo.findByConfirmer(requester).stream().filter(friendship -> friendship.isConfirmed()).map(friendship -> friendship.getRequester()).toList());
        return friends;
    }

    public List<User> getFriends(User requester) {
        List<User> friends = repo.findByRequester(requester).stream().filter(friendship -> friendship.isConfirmed()).map(friendship -> friendship.getConfirmer()).collect(Collectors.toList());
        friends.addAll(repo.findByConfirmer(requester).stream().filter(friendship -> friendship.isConfirmed()).map(friendship -> friendship.getRequester()).toList());
        return friends;
    }

    private Friendship findFriendShip(User first, User second, boolean confirmed) {
        Friendship toReturn = repo.findByConfirmerAndRequesterAndConfirmed(first, second, confirmed);
        if(toReturn !=null) return toReturn;
        toReturn = repo.findByConfirmerAndRequesterAndConfirmed(second, first, confirmed);
        if(toReturn != null) return toReturn;
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Friendship not found");
    }

    public void confirmFriendship(String email) {
        Friendship friendship = findFriendShip( userService.getUserFromToken(), userService.findUserById(email), false);
        repo.save(friendship.confirm());
        pushNotification(friendship);
    }

    public void deleteFriendship(String email){
        Friendship friendShip = findFriendShip(userService.getUserFromToken(), userService.findUserById(email), true);
        repo.delete(friendShip);
        pushNotification(friendShip);
    }

    public void cancelRequest(String email){
        Friendship friendShip = findFriendShip(userService.getUserFromToken(), userService.findUserById(email), false);
        repo.delete(friendShip);
        pushNotification(friendShip);
    }

    public void promoteFriend(String email) {
        userService.promoteUser(email);
    }

    public void demoteFriend(String email) {
        userService.demoteUser(email);
    }
}


