package cz.sralbotka.pia.hatebook.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import cz.sralbotka.pia.hatebook.dao.UserRepository;
import cz.sralbotka.pia.hatebook.entities.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Date;
import java.util.Optional;

/**
 * Singleton that stores all about security
 */
public class SecurityUtils {

    private SecurityUtils(){
        //Singleton
    }

    public static final String SECRET = "aDogWentToBuy55Small#$asd4qweads";
    //TODO Needs to be really 15 minutes.
    public static final long EXPIRATION_TIME = 900_000_000; // 15 mins

    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";

    /**
     * Creates a token from a user
     *
     * @param user - user to create
     * @return token if everything goes smoothly
     */
    public static String createJWTToken(String email){
        return JWT.create()
                .withSubject(email)
                .withExpiresAt(new Date(System.currentTimeMillis() + SecurityUtils.EXPIRATION_TIME))
                .sign(Algorithm.HMAC512(SecurityUtils.SECRET.getBytes()));
    }

    public static Optional<User> getCallerEmailFromToken(UserRepository repo){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication!=null){
            return repo.findById(String.valueOf(authentication.getPrincipal()));
        }
        return Optional.empty();
    }
}
