package cz.sralbotka.pia.hatebook.service;

import cz.sralbotka.pia.hatebook.entities.User;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import java.util.Locale;
import java.util.stream.Stream;

@Service
@Transactional
public class FulltextSearchService implements IFulltextSearchService {

    private final EntityManager em;
    private FullTextEntityManager ftem;

    public FulltextSearchService(EntityManager em) {
        this.em = em;
    }

    @PostConstruct
    public void init() {
        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(em.getEntityManagerFactory().createEntityManager());
        try {
            fullTextEntityManager.createIndexer().startAndWait();
        } catch (InterruptedException e) {
            LoggerFactory.getLogger(this.getClass()).error(e.getLocalizedMessage(), e);
        }
        ftem = fullTextEntityManager;
    }

    @Override
    public Stream<User> fulltextFindUsers(String name) {
        QueryBuilder queryBuilder = ftem.getSearchFactory().buildQueryBuilder().forEntity(User.class).get();
        Query fulltextSearchOnNameIgnoreCase = queryBuilder.keyword().wildcard().onField("name").ignoreFieldBridge().matching("*" + name.toLowerCase(Locale.ROOT) + "*").createQuery();
        return (Stream<User>) ftem.createFullTextQuery(fulltextSearchOnNameIgnoreCase, User.class).getResultStream();
    }
}
