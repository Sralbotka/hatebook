import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import {Link as Rlink} from "react-router-dom";
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import {useState} from 'react';
import {checkEntropy, EntropyType} from '../utilities/Password';
import Done from '@mui/icons-material/Done';
import Close from '@mui/icons-material/Close';
import InputAdornment from '@mui/material/InputAdornment';
import {InputProps} from '@mui/material/Input';
import {Copyright} from "./Copyright";
import {isEmailTaken, signUp} from '../serverCalls/Auth';

interface SignUpValidators {
    passwordOk: boolean,
    emailOk: boolean,
    nameOk: boolean,
    surnameOk: boolean
}


const theme = createTheme();

export default function SignUp() {

    const [entropy, setEntropy] = useState<EntropyType>();
    const [validator, setValidators] = useState<SignUpValidators>({ passwordOk: false, nameOk: false, surnameOk: false, emailOk: false });

    function calculateEntropy(event: React.ChangeEvent<HTMLInputElement>) {
        let entropy = checkEntropy(event.target.value);
        setEntropy(checkEntropy(event.target.value));
        setValidators({...validator, passwordOk: entropy.score > 60})
    }

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);

        const user = {
            name : data.get('firstName') + " " + data.get('lastName'),
            email : data.get('email')+"",
            password : data.get('password')+""
        }
        signUp(user);
    };

    function createEndAdorement(ok: boolean):Partial<InputProps> {
        return {
            endAdornment: <InputAdornment position="start">
                {ok ? <Done /> : <Close />}
            </InputAdornment>
        }
    }

    const nameChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
        
        setValidators({ ...validator, nameOk:!!event.target.value && event.target.value.trim().length > 0})
    }

    const surnameChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
        
        setValidators({ ...validator, surnameOk:!!event.target.value && event.target.value.trim().length > 0})
    }

    const emailChanged = async(event: React.ChangeEvent<HTMLInputElement>) => {
        let value = event.target.value;
        let regex = '^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$';
        let result = false
        if(!!value && value.match(regex)){
            result = value.match(regex) !== null;
            if(result)
            {
                result = await isEmailTaken(value);                
                setValidators({ ...validator, emailOk:!result});                              
            }
        }
        else{
            setValidators({ ...validator, emailOk:result});
        }
        
    }

    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign up
                    </Typography>
                    <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    autoComplete="given-name"
                                    name="firstName"
                                    required
                                    fullWidth
                                    id="firstName"
                                    label="First Name"
                                    autoFocus
                                    InputProps={createEndAdorement(validator.nameOk)}
                                    onChange={nameChanged}                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    required
                                    fullWidth
                                    id="lastName"
                                    label="Last Name"
                                    name="lastName"
                                    autoComplete="family-name"
                                    InputProps={createEndAdorement(validator.surnameOk)}
                                    onChange={surnameChanged}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email Address"
                                    name="email"
                                    autoComplete="email"
                                    InputProps={createEndAdorement(validator.emailOk)}
                                    onChange={emailChanged}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    name="password"
                                    label="Password"
                                    type="password"
                                    id="password"
                                    helperText={`Your password is ${entropy?.translation} with score ${entropy?.score}. https://xkcd.com/936/`}
                                    autoComplete="new-password"
                                    InputProps={createEndAdorement(validator.passwordOk)}
                                    onChange={calculateEntropy}                                    
                                />
                            </Grid>                           
                        </Grid>
                        {validator.emailOk && validator.nameOk && validator.passwordOk && validator.surnameOk && <Button                            
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                        >    Sign Up
                        </Button>}
                        
                        <Grid container justifyContent="flex-end">
                            <Grid item>
                                <Rlink to="/">
                                    
                                        Already have an account? Sign in
                                    
                                </Rlink>
                            </Grid>
                        </Grid>
                    </Box>
                </Box>
                <Copyright sx={{ mt: 5 }} />
            </Container>
        </ThemeProvider>
    );
}