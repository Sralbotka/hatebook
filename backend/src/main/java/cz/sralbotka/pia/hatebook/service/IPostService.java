package cz.sralbotka.pia.hatebook.service;


import cz.sralbotka.pia.hatebook.entities.Postable;

import java.util.List;

public interface IPostService {
    void addPost(String title, String content);

    void addAnnouncement(String title, String content);

    void deletePost(long id);

    List<Postable> getPostsFor(String email);

    List<Postable> getFeed();
}
