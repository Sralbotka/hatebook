import Box from "@mui/material/Box";
import * as React from "react";
import {useEffect, useState} from "react";
import IconButton from "@mui/material/IconButton";
import Toolbar from "@mui/material/Toolbar";
import {Drawer, ListItem, ListItemIcon, ListItemText, Popover, useTheme} from "@mui/material";
import Divider from "@mui/material/Divider";
import List from "@mui/material/List";
import ListItemButton from "@mui/material/ListItemButton";
import PowerIcon from '@mui/icons-material/Power';
import PowerOffIcon from '@mui/icons-material/PowerOff';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import {styled} from "@mui/material/styles";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import {chat, ChatMessage, registerMessageListener} from "../serverCalls/STOMP";
import {chatMap, ChatProps} from "./Chat";
import PeopleIcon from '@mui/icons-material/People';

const drawerWidth = 240;

export function PopupChat(props: ChatProps) {
    const theme = useTheme();
    const [friendListOpened, setFriendListOpened] = useState(false);
    const [chatOpened, setChatOpened] = useState<boolean>(false);
    const [messages, setMessage] = useState<string[]>([]);
    const [to, setTo] = useState("");

    function incomingMessage(message: ChatMessage) {
        if (!chatMap[message.from]) chatMap[message.from] = [];
        chatMap[message.from].push(message.message);
        console.log("---RECEIVED---" + message.from);
        console.log("TO:" + to);
        console.log("UPDATING");
        setMessage([...chatMap[message.from].concat(chatMap[props.from])]);
    }

    useEffect(() => {
        registerMessageListener(incomingMessage);
    }, []);

    function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        let post = {
            message: data.get("content") + "",
            seconds: Date.now() / 1000,
            to: to
        }
        if (!chatMap[to]) chatMap[to] = [];
        chatMap[to].push(post.message);
        setMessage([...chatMap[to].concat(chatMap[props.from])]);
        chat({from: props.from, message: post.message, to: to, timestamp: post.seconds})
        console.log(chatMap);
    }

    function changeChat(email: string) {
        if (!chatMap[email]) chatMap[email] = [];
        setMessage(chatMap[email].concat(chatMap[props.from]));
        setTo(email);
    }

    function hide(): void {
        setFriendListOpened(false);
    }

    function show(): void {
        setFriendListOpened(true);
    }

    function closeChat() {
        setChatOpened(false);
    }

    function openChat(email: string): void {
        setChatOpened(true);
        changeChat(email);
    }


    const DrawerHeader = styled('div')(({theme}) => ({
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    }));

    const chatWindow = (
        <Box sx={{
            display: 'flex',
            flexDirection: 'row',
        }}>
            <Box sx={{
                display: 'flex',
                flexDirection: 'column'
            }}>
                {messages.map((message, index) => <Typography key={`message-${index}`}>{message}</Typography>)}
                <Box component="form" onSubmit={handleSubmit} sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}>
                    <TextField
                        id="conent"
                        label="Hate 1:1"
                        placeholder="Hate 1:1"
                        name="content"
                        multiline
                        fullWidth={true}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{mt: 3, mb: 2}}
                    > Send your hate
                    </Button>
                </Box>
            </Box>
        </Box>
    )

    const popover = (
        <Popover
            id="chat"
            open={chatOpened}
            onClose={closeChat}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
            }}
            sx={{height: '90%'}}
        >
            {chatWindow}
        </Popover>
    )

    const list = (
        <List>
            {props.friends.map((friend) =>
                <ListItem key={friend.minutes + "-item"}>
                    <ListItemButton key={friend.minutes + "-button"}>
                        <ListItemText primary={friend.friend.name}
                                      key={friend.minutes + "-text"}
                                      onClick={event => {openChat(friend.friend.email)}}
                        />
                        <ListItemIcon>{((Date.now() / 1000) - friend.friend.lastSeen) / 60 > 15 ? <PowerOffIcon/> : <PowerIcon/>}</ListItemIcon>
                    </ListItemButton>
                </ListItem>)
            }
        </List>
    );

    return (
        <Box position={"absolute"}>
            <Toolbar>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={show}
                    edge="start"
                    sx={{mr: 2, ...(friendListOpened && {display: 'none'})}}
                >
                    <PeopleIcon/>
                </IconButton>
            </Toolbar>
            <Box sx={{display: 'flex'}}>
                <Box
                    component="nav"
                    sx={{width: {sm: drawerWidth}, flexShrink: {sm: 0}}}
                    aria-label="mailbox folders"
                >
                    {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                    <Drawer
                        sx={{
                            width: drawerWidth,
                            flexShrink: 0,
                            '& .MuiDrawer-paper': {
                                width: drawerWidth,
                                boxSizing: 'border-box',
                            },
                        }}
                        variant="persistent"
                        anchor="left"
                        open={friendListOpened}
                    >
                        <DrawerHeader>
                            <div>Friends</div>

                            <IconButton onClick={hide}>
                                {theme.direction === 'ltr' ? <ChevronLeftIcon/> : <ChevronRightIcon/>}
                            </IconButton>
                        </DrawerHeader>
                        <Divider/>
                        {list}
                    </Drawer>
                </Box>
            </Box>
            {popover}
        </Box>
    )
}