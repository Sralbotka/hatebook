import Autocomplete from "@mui/material/Autocomplete";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import {useState} from "react";
import {User} from "../serverCalls/ServerUtils";
import {HatebookAvatar} from "./HatebookAvatar";
import {reRoute} from "./Navigator";


export interface AutocompleteProps{
  fullTextSearch:(what:string)=>Promise<User[]>;  
}



export function AutoCompleteSearch(props : AutocompleteProps){

  const[users, setUsers] = useState<User[]>([]);

  async function inputChanged(event: React.ChangeEvent<HTMLInputElement>){     
     setUsers(await props.fullTextSearch(event.target.value)); 
  }

  function userSelected(event: React.SyntheticEvent, value: User | null){    
    if(value){
      reRoute(`/user/${value.email}`);
    }
  }

  return (
  <Autocomplete
    isOptionEqualToValue={(option,value) => option.email === value.email}
    onChange={userSelected}
    disablePortal
    id="combo-box-demo"
    options={users}
    sx={{ width: 300 }}
    renderInput={(params) => <TextField {...params} label="Find someone to hate" onChange={inputChanged} variant="standard"/>}
    getOptionLabel={(u) => u.name ? u.name : "This will not happen really"}
    renderOption={(props, option) => (
      <Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props} key={option.email}>
        <HatebookAvatar who={option.name}/>
        {option.name }
      </Box>
    )}
  />);

}