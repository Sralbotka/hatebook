package cz.sralbotka.pia.hatebook.chat;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;
import org.springframework.messaging.simp.SimpMessagingTemplate;

@Configuration
public class ChatConfig {

    @Bean
    public ChatListener chatListener(SimpMessagingTemplate messagingTemplate) {
        ChatListener listener = new ChatListener(messagingTemplate);
        return listener;
    }
}
