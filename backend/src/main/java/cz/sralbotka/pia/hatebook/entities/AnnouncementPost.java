package cz.sralbotka.pia.hatebook.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "announcements")
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class AnnouncementPost extends AutoGeneratedId implements Postable {
    @ManyToOne
    private User author;
    private long unixTimestamp;
    private String title;
    @Column(columnDefinition="TEXT")
    private String message;
}
