package cz.sralbotka.pia.hatebook.controllers;

import cz.sralbotka.pia.hatebook.entities.Friendship;
import cz.sralbotka.pia.hatebook.entities.User;
import cz.sralbotka.pia.hatebook.service.FriendshipService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/friendships")
public class FriendshipController {
    public static final String WRONG_EMAIL = "Sorry that is not an email in the body! Expect to obtain {email:a@b.c";

    private final FriendshipService service;

    public FriendshipController(FriendshipService service) {
        this.service = service;
    }

    /**
     * returns list of friendships
     * @return list of friendships
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Friendship>> getFriendships() {
        return ResponseEntity.ok(service.getFriendships());
    }

    /**
     *
     * @return list of friends if OK
     */
    @RequestMapping(value = "/friends", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getFriends() {
        return ResponseEntity.status(HttpStatus.OK).body(service.getFriends());
    }

    /**
     * Confirms friendship request
     * @param user user whom friendship will be confirmed
     * @return 200 if OK with notice
     */
    @RequestMapping(value = "/confirm", method = RequestMethod.POST)
    public ResponseEntity<String> confirmFriendShip(@RequestBody User user) {
        service.confirmFriendship(user.getEmail());
        return ResponseEntity.status(HttpStatus.OK).body("Friendship Confirmed");
    }

    /**
     * Canceles friendship request
     * @param email user that wont be friend
     * @return 200 if OK with Notice
     */
    @RequestMapping(value="/confirm/{email}", method=RequestMethod.DELETE)
    public ResponseEntity<String> cancelFriendRequest(@PathVariable("email") String email){
        service.cancelRequest(email);
        return ResponseEntity.ok("Friendship request cancelled");
    }

    /**
     * Creates friendship with user
     * @param user that will be friend
     */
    @RequestMapping(method = RequestMethod.POST)
    public void create(@RequestBody User user) {
        if (user != null && user.getEmail() != null) {
            service.makeFriendShip(user.getEmail());
        }
    }

    /**
     * Removes friendship with user
     * @param email of user that wont be your friend
     */
    @RequestMapping(value = "/{email}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("email") String email) {
        service.deleteFriendship(email);

    }

    /**
     * Promotes friend to ADMIN
     * @param email of friend that will be admin
     * @return 200 if OK with notice
     */
    @RequestMapping(value = "/promotion/{email}", method = RequestMethod.POST)
    public ResponseEntity<String> promote(@PathVariable("email") String email){
        service.promoteFriend(email);
        return ResponseEntity.ok("Friend promoted to ADMIN");
    }

    /**
     * Removes ADMIN role from user
     * @param email of demoted friend
     * @return 200 if OK with notice
     */
    @RequestMapping(value = "/promotion/{email}", method = RequestMethod.DELETE)
    public ResponseEntity<String> demote(@PathVariable("email") String email){
        service.demoteFriend(email);
        return ResponseEntity.ok("Friend promoted to ADMIN");
    }
}
