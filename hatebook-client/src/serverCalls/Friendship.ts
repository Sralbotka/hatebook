import axios from "axios";
import {setAuth} from "../App";
import {displayError, displaySuccess} from "../utilities/ErrorHandler";
import {getEmptyUserInfo, SERVER, User, UserInfo} from "./ServerUtils";
import {ChatMessage, fireUpSocket} from "./STOMP";
import {fillChatMessages} from "../components/Chat";

interface Friendship {
    confirmed: boolean,
    minutes: number,
    requester: User,
    confirmer: User
}

let _info: UserInfo;

export function populateAfterAuth(info: UserInfo) {
    axios.get<User[]>(`${SERVER}/ban/all`).then((response) => {
        extractListFromBans(response.data, info)
    }).catch(e => displayError("Can't fetch ban list"));
    axios.get<Friendship[]>(`${SERVER}/friendships`).then((response) => {
        extractListsFromFriendship(response.data, info)
    }).catch(e => displayError("Can't fetch friends"));
    axios.get<ChatMessage[]>(`${SERVER}/messages/${info.email}`).then((response) => fillChatMessages(response.data, info.email)).catch(e => displayError("Can't read messages history"));
    fireUpSocket(info.email);
}

function extractListFromBans(users: User[], info: UserInfo) {
    info.blockedByMe = users;
}

function extractListsFromFriendship(friendships: Friendship[], info: UserInfo) {
    friendships.forEach(friendship => {
        if (friendship.confirmed) {
            let friend = friendship.confirmer.email === info.email ? friendship.requester : friendship.confirmer;
            info.friends.push({friend: friend, minutes: friendship.minutes});
        } else if (friendship.confirmer.email === info.email) info.requestMyConfirmation.push({
            friend: friendship.requester,
            minutes: friendship.minutes
        })
        else info.iRequestedTheirConfirmation.push({friend: friendship.confirmer, minutes: friendship.minutes});

    });
    setAuth(info);
    _info = info;
    console.log(info);
}

export function confirmFriend(email: string) {
    axios.post(`${SERVER}/friendships/confirm`, {email: email}).then(() => {
        displaySuccess("Request sent wait for confirmation");

    }).catch((e) => displayError("Can't add friend")).then(() => populateAfterAuth({..._info}));
}

export function cancelFriendshipRequest(email: string) {
    axios.delete(`${SERVER}/friendships/confirm/${email}`).then(() => displaySuccess("Friendship request cancelled succesfully - thanks god it could have been awkward...")).catch((e) => displayError("Can't cancel friendship")).then(refreshAfterUpdate);
}

export function addFriend(email: string) {
    axios.post(`${SERVER}/friendships`, {email: email}).then(() => displaySuccess("Request sent wait for confirmation")).catch((e) => displayError("Can't add friend")).then(refreshAfterUpdate);
}

export function removeFriend(email: string) {
    axios.delete(`${SERVER}/friendships/${email}`).then(() => displaySuccess("Friend deleted, poor friend")).catch((e) => displayError("Can't delete friend")).then(refreshAfterUpdate);
}

export function promoteUser(email: string) {
    axios.post(`${SERVER}/friendships/promotion/${email}`).then(() => displaySuccess("User promoted to ADMIN")).catch((e) => displayError("Can't promote friend to ADMIN")).then(refreshAfterUpdate);
}

export function demoteUser(email: string) {
    axios.delete(`${SERVER}/friendships/promotion/${email}`).then(() => displaySuccess("Admin rights revoked. Bwuhahaha")).catch((e) => displayError("Can't revoke ADMIN rights")).then(refreshAfterUpdate);
}

export function banUser(email: string) {
    axios.post(`${SERVER}/ban/${email}`).then(() => displaySuccess("User banned")).catch((e) => displayError("Can't ban user")).then(refreshAfterUpdate);
}

export function unbanUser(email: string) {
    axios.delete(`${SERVER}/ban/${email}`).then(() => displaySuccess("User unbanned")).catch((e) => displayError("Can't unban user")).then(refreshAfterUpdate);
}

export function refreshAfterUpdate(): void {
    return populateAfterAuth({
        ...getEmptyUserInfo(),
        name: _info.name,
        lastSeen: _info.lastSeen,
        email: _info.email,
        permission: _info.permission
    });
}