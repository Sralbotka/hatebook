package cz.sralbotka.pia.hatebook.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Dummy controller to check if authentication is working :)
 */
@RestController
@RequestMapping("/hello")
public class HelloController {

    /**
     * Tester controller
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String hello(){
        return "Hello World";
    }
}
