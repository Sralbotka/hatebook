package cz.sralbotka.pia.hatebook.service;

import cz.sralbotka.pia.hatebook.dao.AnnouncementRepository;
import cz.sralbotka.pia.hatebook.dao.PostRepository;
import cz.sralbotka.pia.hatebook.entities.AnnouncementPost;
import cz.sralbotka.pia.hatebook.entities.Postable;
import cz.sralbotka.pia.hatebook.entities.RegularPost;
import cz.sralbotka.pia.hatebook.entities.User;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class PostService implements IPostService {

    private PostRepository postsRepo;
    private AnnouncementRepository announcementRepo;
    private UserService userService;
    private FriendshipService friendshipService;
    private SimpMessagingTemplate msgTemplate;

    @Override
    public void addPost(String title, String content) {
        User requester = userService.getUserFromToken();
        postsRepo.save(new RegularPost(requester, System.currentTimeMillis() / 1000, title, content));
        friendshipService.getFriends().stream().forEach((user) -> msgTemplate.convertAndSend("/topic/posts/" + user.getEmail(), "New post"));
    }

    @Override
    public void addAnnouncement(String title, String content) {
        User requester = userService.getUserFromToken();
        announcementRepo.save(new AnnouncementPost(requester, System.currentTimeMillis() / 1000, title, content));
        friendshipService.getFriends().stream().forEach((user) -> msgTemplate.convertAndSend("/topic/posts/" + user.getEmail(), "New post"));
    }

    @Override
    public void deletePost(long id) {
        Postable post = postsRepo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Provided post doesn't exist..."));
        if (post.getAuthor().getEmail().equals(userService.getUserFromToken().getEmail()))
            postsRepo.delete((RegularPost) post);
        else
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can't delete post that you are not author of...");
    }

    @Override
    public List<Postable> getPostsFor(String email) {
        User author = userService.findUserById(email);
        List<Postable> posts = new ArrayList<>();
        if(userService.getUserFromToken().getEmail().equals(email) || friendshipService.getFriends().contains(author))
            posts.addAll(postsRepo.findByAuthor(author).stream().sorted(getPostComparator()).toList());
        posts.sort(getPostComparator().reversed());
        return posts;
    }

    private Comparator<Postable> getPostComparator() {
        return Comparator.comparingLong(Postable::getUnixTimestamp).reversed();
    }

    @Override
    public List<Postable> getFeed() {
        List<Postable> posts =
                Stream.concat(
                        friendshipService.getFriends().stream().flatMap((user) -> postsRepo.findByAuthor(user).stream()),
                        postsRepo.findByAuthor(userService.getUserFromToken()).stream()
                ).collect(Collectors.toList());
        posts.addAll(announcementRepo.findAll());
        posts.sort(getPostComparator());
        return posts;
    }
}
