import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardHeader from "@mui/material/CardHeader";
import Divider from "@mui/material/Divider";
import Typography from "@mui/material/Typography";
import {
    cancelFriendshipRequest,
    confirmFriend,
    demoteUser,
    promoteUser,
    removeFriend,
    unbanUser
} from "../serverCalls/Friendship";
import {Permission, User, UserInfo} from "../serverCalls/ServerUtils";
import {HatebookAvatar} from "./HatebookAvatar";
import {reRoute} from "./Navigator";
import * as React from "react";

export interface FriendshipManagementProps {
    auth: UserInfo,
}

interface UserCardProps {
    name: string,
    buttons: JSX.Element[],
    lastChange: number;
}

export function FriendshipManagement(props: FriendshipManagementProps) {

    function createFriendButtons(friend: User, permission: Permission): JSX.Element[] {
        let buttons: JSX.Element[] = [];
        buttons.push(<Button onClick={() => removeFriend(friend.email)}>Unfriend</Button>)
        if (permission == Permission.ADMIN) {
            if (friend.permission == Permission.USER)
                buttons.push(<Button onClick={() => promoteUser(friend.email)}>Promote to ADMIN</Button>)
            if (friend.permission == Permission.ADMIN)
                buttons.push(<Button onClick={() => demoteUser(friend.email)}>Revoke ADMIN</Button>)
        }
        return buttons;
    }

    function createUserCard(props: UserCardProps) {
        return (<Card sx={{maxWidth: 600}}>
            <CardHeader
                avatar={<HatebookAvatar who={props.name}/>}

                title={props.name}
                subheader={`Received at ${props.lastChange}`}
            />
            <CardActions disableSpacing>
                <Button></Button>
                {props.buttons}
            </CardActions>
        </Card>);
    }

    function createClickableUserCard(props: UserCardProps, email: string) {
        return (<Card sx={{maxWidth: 600}}>
            <CardHeader
                avatar={<HatebookAvatar who={props.name}/>}
                onClick={() => {reRoute(`/user/${email}`)}}
                title={props.name}
                subheader={`Received at ${props.lastChange}`}
            />
            <CardActions disableSpacing>
                <Button></Button>
                {props.buttons}
            </CardActions>
        </Card>);
    }

    function createUnbanButtons(user: User): JSX.Element[] {
        let buttons: JSX.Element[] = [];
        buttons.push(<Button onClick={() => unbanUser(user.email)}>unban user</Button>)
        return buttons;
    }


    return (<Box sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    }}>
        <Typography>Friends:</Typography>
        {props.auth.friends.map((friend) => createClickableUserCard({
            name: friend.friend.name,
            lastChange: friend.minutes,
            buttons: createFriendButtons(friend.friend, props.auth.permission)
        }, friend.friend.email))}
        <Divider/>
        <Typography>Friend requests:</Typography>
        {props.auth.requestMyConfirmation.map((friend) => createUserCard({
            name: friend.friend.name,
            lastChange: friend.minutes,
            buttons: createAcceptButtons(friend.friend.email)
        }))}
        <Typography>Pending requests:</Typography>
        {props.auth.iRequestedTheirConfirmation.map((friend) => createUserCard({
            name: friend.friend.name,
            lastChange: friend.minutes,
            buttons: createDeleteRequestButtons(friend.friend.email)
        }))}
        <Typography>Banned users:</Typography>
        {props.auth.blockedByMe.map((user) => createUserCard({
            name: user.name,
            lastChange: user.lastSeen,
            buttons: createUnbanButtons(user)
        }))}
    </Box>)


}

function createAcceptButtons(email: string): JSX.Element[] {
    return [<Button onClick={() => confirmFriend(email)}>Confirm Friendship</Button>,
        <Button onClick={() => cancelFriendshipRequest(email)}>Reject Friendship</Button>]
}


function createDeleteRequestButtons(email: string): JSX.Element[] {
    return [<Button onClick={() => cancelFriendshipRequest(email)}>Take it back</Button>]
}

