package cz.sralbotka.pia.hatebook.dao;

import cz.sralbotka.pia.hatebook.entities.Message;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MessageRepository extends CrudRepository<Message, Long> {

    List<Message> findTop10ByFromEmailAndToEmailOrderBySecondsDesc(String senderEmail, String toEmail);
    List<Message> findTop10ByToEmailAndFromEmailOrderBySecondsDesc(String toEmail, String senderEmail);

}
