package cz.sralbotka.pia.hatebook.controllers;

import cz.sralbotka.pia.hatebook.service.FriendshipService;
import cz.sralbotka.pia.hatebook.service.IUserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/search")
public class SearchController extends AbstractUserController {

    private final FriendshipService friendshipService;

    public SearchController(IUserService userService, FriendshipService friendshipService) {
        super(userService);
        this.friendshipService = friendshipService;
    }

    private record SearchRecord(String text) {
    }

    private record UserRecord(String name, String email) {
    }


    /**
     * Fulltext search of users
     *
     * @param search search record that will be queried
     * @return List of users that are matched
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<List<UserRecord>> search(@RequestBody SearchRecord search) {
        return ResponseEntity.status(200).body(
                userService.searchUsers(search.text())
                        .stream()
                        .filter((user) -> !friendshipService.getFriends().contains(user))
                        .map(u -> new UserRecord(u.getName(), u.getEmail()))
                        .collect(Collectors.toList()));
    }
}
