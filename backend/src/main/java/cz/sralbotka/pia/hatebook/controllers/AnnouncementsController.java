package cz.sralbotka.pia.hatebook.controllers;

import cz.sralbotka.pia.hatebook.dto.PostDTO;
import cz.sralbotka.pia.hatebook.service.IPostService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/announcements")
@AllArgsConstructor
public class AnnouncementsController {

    private IPostService service;

    /**
     * Adds announcement to the feed
     * @param post PostDTO -> announcement
     */
    @RequestMapping(method = RequestMethod.POST)
    public void addAnnouncement(@RequestBody PostDTO.PostRecord post) {
        service.addAnnouncement(post.title(), post.message());
    }
}
