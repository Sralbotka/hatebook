import {WS_SERVER} from "./ServerUtils";
import {Client} from '@stomp/stompjs';
import {refreshAfterUpdate} from "./Friendship";

export interface ChatMessage {
    from: string,
    to: string,
    message: string,
    timestamp: number
}

let messageListener: (callback: ChatMessage) => void;
let postRefreshListener: () => void;

const client = new Client({
    brokerURL: `ws://${WS_SERVER}/ws`,
    connectHeaders: {
        login: 'user',
        passcode: 'password',
        Authorization: ""
    },
    debug: function (str) {
        console.log(str);
    },
    reconnectDelay: 5000,
    heartbeatIncoming: 4000,
    heartbeatOutgoing: 4000,
});

client.onConnect = function (frame) {
    // Do something, all subscribes must be done is this callback
    // This is needed because this will be executed after a (re)connect
};

client.onStompError = function (frame) {
    // Will be invoked in case of error encountered at Broker
    // Bad login/passcode typically will cause an error
    // Complaint brokers will set `message` header with a brief message. Body may contain details.
    // Compliant brokers will terminate the connection after any error
    console.log('Broker reported error: ' + frame.headers['message']);
    console.log('Additional details: ' + frame.body);
};


let interval: NodeJS.Timeout;

export function fireUpSocket(email: string) {
    document.cookie = "Hovno=prdelsrack;path=/";
    client.activate();
    client.onConnect = () => {
        client.subscribe("/topic/greetings", (message) => console.log(message.body));
        client.subscribe(`/topic/chat/${email}`, (message) => {
            if (messageListener) messageListener(JSON.parse(message.body));
            console.log(message.body)
        });
        client.subscribe(`/topic/posts/${email}`, (message) => {
            postRefreshListener();
        });
        client.subscribe(`/topic/friendship/${email}`, () => {
            setTimeout(refreshAfterUpdate, 1000);
        });
        client.subscribe(`/topic/chat/online/${email}`, () => {
            refreshAfterUpdate();
        });
        interval = setInterval(() => {
            client.publish({
                destination: "/app/hello",
                body: "NESER",
            })
        }, 20000000);


    };


}

export function chat(message: ChatMessage) {
    if (client && client.connected) {
        client.publish({
            destination: `/app/chat/${message.from}/${message.to}`,
            body: JSON.stringify(message)
        })
    }
}

export function killSocket() {
    client.deactivate();
    clearInterval(interval);
}


export function registerMessageListener(callback: (message: ChatMessage) => void) {
    messageListener = callback;
}

export function registerPostRefreshListener(callback: () => void) {
    postRefreshListener = callback;
}
  