package cz.sralbotka.pia.hatebook.controllers;

import cz.sralbotka.pia.hatebook.dto.PostDTO;
import cz.sralbotka.pia.hatebook.entities.Postable;
import cz.sralbotka.pia.hatebook.service.IPostService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/posts")
@AllArgsConstructor
public class PostController {

    private IPostService service;

    /**
     * gets user feed and returns it to requester
     * @return List of Postable - feed
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<PostDTO.PostRecord>> getFeed() {
        return ResponseEntity.status(HttpStatus.OK).body(convertToPostRecord(service.getFeed()));
    }


    /**
     * adds user post to db
     * @param post post that will be safe to DB
     */
    @RequestMapping(method = RequestMethod.POST)
    public void addPost(@RequestBody PostDTO.PostRecord post) {
        service.addPost(post.title(), post.message());
    }

    /**
     * get posts (feed) for defined user (only posts, not announcements)
     * @param email of user
     * @return posts of user (not announcements)
     */
    @RequestMapping(value = "/{email}", method = RequestMethod.GET)
    public ResponseEntity<List<PostDTO.PostRecord>> getPostsForUser(@PathVariable("email") String email) {
        return ResponseEntity.
                status(HttpStatus.OK).
                body(service.getPostsFor(email).stream().map(getRecordFromPost()).collect(Collectors.toList()));
    }

    private List<PostDTO.PostRecord> convertToPostRecord(List<Postable> posts) {
        return posts.stream().map(getRecordFromPost()).collect(Collectors.toList());
    }


    private Function<Postable, PostDTO.PostRecord> getRecordFromPost() {
        return (post) -> new PostDTO.PostRecord(post.getTitle(), post.getMessage(), post.getAuthor().getName(), post.getUnixTimestamp());
    }
}
