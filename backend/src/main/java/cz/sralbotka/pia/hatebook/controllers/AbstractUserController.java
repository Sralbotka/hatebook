package cz.sralbotka.pia.hatebook.controllers;

import cz.sralbotka.pia.hatebook.entities.User;
import cz.sralbotka.pia.hatebook.service.IUserService;

public abstract class AbstractUserController {

    protected IUserService userService;

    public AbstractUserController(IUserService userService) {
        this.userService = userService;
    }

    protected boolean hasValidEmail(User u) {
        return u != null && u.getEmail() != null && u.getEmail().matches("^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$");
    }

    protected boolean hasEmailNameAndPassword(User u) {
        return hasValidEmail(u) && u.getName() != null && !u.getName().isEmpty() && u.getPassword() != null && !u.getPassword().isEmpty();
    }

}
