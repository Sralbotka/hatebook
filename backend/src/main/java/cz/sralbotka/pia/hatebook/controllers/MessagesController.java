package cz.sralbotka.pia.hatebook.controllers;

import cz.sralbotka.pia.hatebook.beans.MessageBean;
import cz.sralbotka.pia.hatebook.service.MessagingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/messages")
public class MessagesController {
    private final MessagingService messagingService;

    MessagesController(MessagingService service) {
        this.messagingService = service;
    }

    /**
     * Gets chat history of a user (max 10 messages)
     * @param email email of user
     * @return list of messages
     */
    @RequestMapping(value="/{email}", method= RequestMethod.GET)
    public ResponseEntity<List<MessageBean>> getChatHistory(@PathVariable("email") String email){
        return ResponseEntity.status(HttpStatus.OK).body(messagingService.getChatHistory(email));
    }

    /**
     * Notify all friends that user is online (over websocket - sends them message to refresh)
     * @param email email of user that is notifying he's online
     * @return 200 if OK with true in body
     */
    @RequestMapping(value="/notifyAll", method = RequestMethod.POST)
    public ResponseEntity<Boolean> notifyAll(String email) {
        messagingService.notifyFriends();
        return ResponseEntity.status(HttpStatus.OK).body(true);
    }
}
