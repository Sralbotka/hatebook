package cz.sralbotka.pia.hatebook.beans;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class MessageBean {
    private String from;
    private String to;
    private long seconds;
    private String message;
}
