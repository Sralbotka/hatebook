package cz.sralbotka.pia.hatebook.dto;

public class PostDTO {
    public record PostRecord(String title, String message, String author, long seconds) {
    }
}
