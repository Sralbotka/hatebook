package cz.sralbotka.pia.hatebook.entities;

public interface Postable {
    public User getAuthor();

    public long getUnixTimestamp();

    public String getTitle();

    public String getMessage();
}
