package cz.sralbotka.pia.hatebook.controllers;

import cz.sralbotka.pia.hatebook.beans.MessageBean;
import cz.sralbotka.pia.hatebook.beans.TokenBean;
import cz.sralbotka.pia.hatebook.entities.Permission;
import cz.sralbotka.pia.hatebook.service.MessagingService;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class MessagingController {
    private final MessagingService messagingService;

    MessagingController(MessagingService service) {
        this.messagingService = service;
    }


    /**
     * testing
     * @return new TokenBean with tester user
     */
    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public TokenBean test() {
        return new TokenBean("test", "test", "test@test.tst", Permission.USER);
    }

    /**
     * Sends messages between user in chat
     * @param from sender
     * @param to receiver
     * @param messageBean message
     * @return message
     */
    @MessageMapping("/chat/{from}/{to}")
    @SendTo("/topic/chat/{to}")
    public MessageBean message(@DestinationVariable String from, @DestinationVariable String to, MessageBean messageBean) {
        messagingService.addMessage(from, to, messageBean.getMessage());
        return messageBean;
    }

    /**
     * notification for user when his friend is online - refresh notification
     * @param email email of the one that will be notified
     * @return just true
     */
    @SendTo("/topic/chat/online/{email}")
    public boolean message(@DestinationVariable String email) {
        return true;
    }

    /**
     * listener to post refresh - receiver should refresh posts
     * @return just true
     */
    @SendTo("/topic/posts/refresh")
    public boolean sendRefresh() {
        return true;
    }
}
