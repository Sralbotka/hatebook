

export function nameForAvatar(name:string):string{
    return name.split(" ").reduce((previous,current) => previous.substring(0,1) + current.substring(0,1));    
}