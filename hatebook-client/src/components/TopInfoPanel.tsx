import * as React from 'react';
import Snackbar from '@mui/material/Snackbar';


export interface SnackBarProps{
  opened: boolean,  
  message: JSX.Element,
  onClose: ()=>void
}

export function SimpleSnackbar(props:SnackBarProps) {       
        

    return (
      
        <Snackbar
          open={props.opened}
          anchorOrigin={{vertical: 'top', horizontal: 'center'}}
          autoHideDuration={6000}
          onClose={props.onClose}                   
        >
          {props.message}
        </Snackbar>
      
    );
  }