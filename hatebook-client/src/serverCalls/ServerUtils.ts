export const SERVER = process.env.REACT_APP_SERVER;
export const WS_SERVER = process.env.REACT_APP_WS_SERVER;

export interface UserForRegistration{
    name?:string,    
    password:string,
    email:string
}

export interface User{
    name:string,
    email:string,
    permission: Permission,
    lastSeen: number
}

export interface TokenType extends User{
    token:string,    
}

export interface FriendRequest{
    friend: User,
    minutes: number
}

export enum Permission {
    USER = "USER",
    ADMIN = "ADMIN",
}

export interface UserInfo extends User{
    friends: FriendRequest[],
    requestMyConfirmation: FriendRequest[],
    iRequestedTheirConfirmation: FriendRequest[],
    blockedByMe: User[]
}

export function getEmptyUserInfo():UserInfo{
    return {name:"", email:"", permission:Permission.USER, lastSeen:0, blockedByMe:[], friends:[], iRequestedTheirConfirmation: [], requestMyConfirmation:[]};
}