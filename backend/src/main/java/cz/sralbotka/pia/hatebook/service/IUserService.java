package cz.sralbotka.pia.hatebook.service;

import cz.sralbotka.pia.hatebook.beans.TokenBean;
import cz.sralbotka.pia.hatebook.entities.Permission;
import cz.sralbotka.pia.hatebook.entities.User;

import java.util.List;

/**
 * Service that handles all users related troubles
 *
 * Please note that each method shall either return a requested value or throws {{@link org.springframework.web.server.ResponseStatusException}}
 *
 */
public interface IUserService {
    /**
     * Adds a user with a valid email, password and a name - check needs to be done before calling on controller
     * @param user - user to add
     */
    void addUser(User user);

    /**
     * Searches for a users matching the fulltext name search following this rules:
     *
     * will return users with names containing the provided text
     * must exclude users who have blocked the searching user (ban)
     * must exclude users who are already friends (friends)
     * must include users with pending friend requests (pending)
     *
     * @param nameSearch
     */
    List<User> searchUsers(String nameSearch);

    /**
     * Bans a user with provided email
     * @param email - email of a user to ban
     */
    boolean banUser(String email);

    /**
     * Unbans a user with provided email
     * @param email - email of users to unban
     */
    boolean unbanUser(String email);

    /**
     * Promotes a user to ADMIN (Only ADMIN can do this!)
     *
     * @param email - email of user to promote
     */
    void promoteUser(String email);

    /**
     * Demotes user from ADMIN to USER (Only ADMIN can do this)
     *
     * @param email - email of a user to demote
     */
    void demoteUser(String email);

    /**
     * Authenticates a user providing a JWT token
     *
     * @param email - email to authenticate
     * @param password - password to authenticate
     * @param permission - role of the user
     * @return JWT token if auth ok otherwise will throw exception
     */
    TokenBean authUser(String email, String password, Permission permission);

    boolean isEmailTaken(String email);

    User getUserFromToken();

    boolean isAdminGranted();

    void timestampUser(User user);
}
