import axios, {AxiosResponse} from "axios";
import {setAuth} from "../App";
import {PostType} from "../components/Feed";
import {reRoute} from "../components/Navigator";
import {displayError, displaySuccess} from "../utilities/ErrorHandler"
import {populateAfterAuth} from "./Friendship";
import {getEmptyUserInfo, SERVER, TokenType, User, UserForRegistration} from "./ServerUtils";
import {killSocket} from "./STOMP";


let token: string;

axios.interceptors.request.use((config) => {
    if (config && config.headers && token && token.length > 0) {
        config.headers.Authorization = `Bearer ${token}`;        
    }
    return config;
})

export async function isEmailTaken(email: string): Promise<boolean> {

    try {
        let result = (await axios.get(`${SERVER}/emailTaken?email=${email}`)).data;
        if (result) {
            displayError(`Email ${email} is already taken.`);
        }
        else {
            displaySuccess(`Email ${email} is free for use.`);

        }
        return result;
    }
    catch (e) {
        displayError("Can't read email from server.")
        return true;
    }
}

export function signUp(user: UserForRegistration) {
    axios.post(`${SERVER}/register`, user).then((result) => {
        displaySuccess(`Account for ${user.email} created. Welcome ${user.name} we will try to auth you now!`);
        signIn(user);
    }).catch(e => displayError("Can't register user - is the server online? Are you hacking my app?"));
}

export function signIn(user: UserForRegistration) {
    axios.post<TokenType>(`${SERVER}/login`, user).then((result) => {        
        displaySuccess(`Authentication succesfull, welcome!`);
        token = result.data.token;
        let userInfo = {...getEmptyUserInfo(), email:result.data.email, lastSeen: result.data.lastSeen, name : result.data.name, permission : result.data.permission};
        setAuth(userInfo);
        populateAfterAuth(userInfo);
        axios.post(`${SERVER}/messages/notifyAll`, user.email).then(
            r => console.log("Notifying friends")
        );
        reRoute("/feed");
    }).catch(e => {
        displayError(`Auth failed ! If this helps - ${e.message}`)
        console.log(e);
        reRoute("/");
    })
}

export function getPostsFor(email: string): Promise<AxiosResponse<PostType[]>> {
    if (email.length === 0) {
        return axios.get(`${SERVER}/posts`);
    }
    return axios.get(`${SERVER}/posts/${email}`);
}

export function addPost(post: PostType): Promise<void> {
    return axios.post(`${SERVER}/posts`, post).then(() => displaySuccess("Post added")).catch((e) => displayError("Can't add post " + e.message))
}

export function addAnnouncement(post: PostType): Promise<void> {
    return axios.post(`${SERVER}/announcements`, post).then(() => displaySuccess("Announcement added successfully")).catch((e) => displayError("Can't add announcement " + e.message))
}

export async function search(what: string): Promise<User[]> {
    try {
        let result = await axios.post(`${SERVER}/search`, { text: what });
        return result.data ? result.data : [];
    }
    catch (e) {
        return [];
    }
}

export function logout(){
    token = "";
    killSocket();
    setAuth(getEmptyUserInfo());
    displaySuccess("Logout successfully")
    reRoute("/");
}