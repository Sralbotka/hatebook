package cz.sralbotka.pia.hatebook.service;

import cz.sralbotka.pia.hatebook.entities.User;

import java.util.stream.Stream;

public interface IFulltextSearchService {
    Stream<User> fulltextFindUsers(String name);
}
