import React, {useState} from 'react';
import './App.css';
import SignIn from './components/SingIn';
import SignUp from "./components/SignUp";
import {Route, Routes} from 'react-router-dom';
import {SimpleSnackbar} from './components/TopInfoPanel';
import Alert from '@mui/material/Alert';
import {Navigator} from "./components/Navigator"
import {TopMenu} from './components/AppBar';
import {Feed} from './components/Feed';
import {getPostsFor} from './serverCalls/Auth';
import {UserDetail} from './components/UserDetail';
import {getEmptyUserInfo, UserInfo} from './serverCalls/ServerUtils';
import {Chat} from './components/Chat';
import {FriendshipManagement} from './components/FriendshipManagement';


let displaySnack:(alert:JSX.Element) => void;
let setAuth:(auth:UserInfo)=>void;


function App() {  

  const[snackOpened, setSnackOpened] = useState(false);
  const onClose = () => setSnackOpened(false);
  const[message, setMessage] = useState(<Alert severity="success">Welcome to Hatebook</Alert>);
  const[auth, setAuthState] = useState<UserInfo>(getEmptyUserInfo());
  
  
  displaySnack = (alert:JSX.Element) =>{    
    setSnackOpened(true);
    setMessage(alert);
  }
  setAuth = setAuthState;  

  let props = {message:message, opened:snackOpened, onClose: onClose}

  return (
    <div>
     <SimpleSnackbar {...props}/>
     <Navigator/>
     <TopMenu auth={auth.email.length > 0}/>
      <Routes>          
        <Route path="/" element={<SignIn/>} key="signIn"/>
        <Route path="/signUp" element={<SignUp/>} key="signUp"/>
        <Route path="/feed" element={<Feed email="" fetchPosts={getPostsFor} auth={auth}/>} key="feed"/>
        <Route path="/user/:id" element={<UserDetail email={auth.email} fetchPosts={getPostsFor} auth={auth}/>} key="detail"/>
        <Route path="/chat" element={<Chat friends={auth.friends} from={auth.email}/>} key="chat"/>
        <Route path="/friendship" element={<FriendshipManagement auth={auth} />} key="friendship"/>
      </Routes>
    </div>
    
  );  
}

export {displaySnack, setAuth}

export default App;
